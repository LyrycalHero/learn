Не забуваємо активувати віртуальне оточення. 
якщо вінда і не стоврена: 
    
    python -m venv venv

активувати: 
    
    .\venv\Script\activate

Охох, якщо вспливає помилка то тре робити наступним чином:  відкриваємо PowerShell з правами адміністратора та вводимо команду нижче щоб вінда нам запускала скріпти 

    Set-ExecutionPolicy RemoteSigned

https://docs.python.org/uk/3/library/gettext.html

Далі в проекті: 

    from gettext import translation
    my_translator = translation('this_project', localedir='locale', languages=['uk'], fallback=True)
    _ = my_translator.gettext 

Де localedir='locale' - посилання на директорію з перекладами, які створимо пізніше 
можна додати fallback=True  - якщо обрати мову якої не існує то не видасть помилку 
_ = my_translator.gettext  - тут відбуватиметься переклад ? 


Далі робота з терміналом (venv)  (щоб очистити термінал можка Ctrl+l)

    cd .\translate\
    xgettext -i .\main.py -o trans.pot -d this_project

Якщо не працює на вінду тре знайти в неті і скачати gettext0.21-iconv1.16-static
Та xgettext -i .\main.py -o trans.pot 

Після цього в файлі trans.pot charset=CHARSET\n міняємо на charset=UTF-8\n

Далі створємо діректорії де буде саме наш переклад для кожної мови 

     mkdir -p locale\uk\LC_MESSAGES

папка LC_MESSAGES обов'язково саме так 

Далі 

    msginit -i .\trans.pot -o .\locale\uk\LC_MESSAGES\this_project.po -l uk

І Вже в файлі this_project.po вставляємо переклади в msgstr 
    
    msgid "Enter your name >>> "
    msgstr "Введіть Ваше ім'я, пане >>> "

І як зробили переклад треба його компілювати в байт код 

    msgfmt .\locale\uk\LC_MESSAGES\this_project.po -o .\locale\uk\LC_MESSAGES\this_project.mo

Якщо з'являються нові слова, що треба перекласти можна оновляти переклади а саме заново онлвюємо наш головий файл
xgettext -i .\main.py -o trans.pot -d this_project: 
    
    xgettext -i .\main.py -o trans.pot -d this_project
    msgmerge --update .\locale\uk\LC_MESSAGES\this_project.po .\trans.pot
    
Дідтянулись нові слова, перекладаємо в this_project.po і далі заново 

    msgfmt .\locale\uk\LC_MESSAGES\this_project.po -o .\locale\uk\LC_MESSAGES\this_project.mo

    