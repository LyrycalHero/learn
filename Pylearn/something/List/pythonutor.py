#Выведите все элементы списка с четными индексами (то есть A[0], A[2], A[4], ...).
#моє:
a = input()
new_list = a.split()

for i in range(len(new_list)):
    if i%2 == 0:
        print(new_list[i], end = ' ')


#Краще від розробників:
a = input().split()
for i in range(0, len(a), 2):
    print(a[i])


#Дан список чисел. Если в нем есть два соседних элемента одного знака, выведите эти числа.
#Если соседних элементов одного знака нет — не выводите ничего. Если таких пар соседей несколько — выведите первую пару.
new_list = [int(s) for s in input().split()]
# можна:
n = 0
while n < len(new_list)-1:
    if (new_list[n] >=0 and new_list[n+1] >= 0) or (new_list[n] <0 and new_list[n+1] < 0):
        print(new_list[n],new_list[n+1])
        break
    n += 1
#також можна математично ( - * - == + і через фор :
for i in range(1, len(new_list)):
    if new_list[i - 1] * new_list[i] > 0:
        print(new_list[i - 1], new_list[i])
        break


my_list = [int(s) for s in input().split()]
k, c = [int(s) for s in input().split()]

last = my_list[-1]

for i in range(len(my_list) - 1, k-1, -1):
    my_list[i ] = my_list[i-1]
my_list[k] = c
my_list.append(last)

print(" ".join([str(s) for s in my_list]))

"""Дан список чисел. Посчитайте, сколько в нем пар элементов, равных друг другу. Считается, что любые два элемента,
 равные друг другу образуют одну пару, которую необходимо посчитать. """
a = [int(s) for s in input().split()]
counter = 0
for i in range(len(a)):
    for j in range(i + 1, len(a)):
        if a[i] == a[j]:
            counter += 1
print(counter)

