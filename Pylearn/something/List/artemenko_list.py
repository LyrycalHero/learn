import random
from fractions import Fraction


def task_2() -> list:
    output_list = [random.randint(163, 190) for item in range(12)]
    return output_list


def task_3() -> list:
    output_list = []
    some_list = [item + 1 for item in range(10)]
    for item in some_list:
        if some_list.index(item) % 2 == 0:
            output_list.append(item)
    return output_list


def task_4() -> None:
    some_list = [random.randrange(10) for item in range(10)]
    output_list = some_list[::-1]  # output_list = some_list.copy()  output_list.reverse()
    print(f'\nВихідний масив: {some_list}\n'
          f'Масив в зворотньому порядку: {output_list}')


def task_5() -> None:
    sum_item = 0
    some_list = [int(input('Введіть значення для массиву: ')) for item in range(12)]
    for item in some_list:
        sum_item += item
    print(f'Сумма елементів = {sum_item}')


def task_6() -> None:
    resistance_list = [random.randrange(2, 20) for item in range(20)]
    print(f'Масив супротиву електричних елементів: {resistance_list} ')
    resistance = 0
    resistance_fr = 0
    for item in resistance_list:
        resistance += 1 / item
        resistance_fr += Fraction(1, item)
    print(f'Супротив електричного ланцюга = {resistance_fr} = {resistance}')


def task_7(m: int, n: int) -> None:
    some_list = [random.randrange(10) for item in range(7)]
    print(f'Поточний массив: {some_list}')
    temp = some_list[n]
    some_list[n] = some_list[m]
    some_list[m] = temp
    print(f'Поміняли {m} та {n} елементи місцями: {some_list}')


def main() -> None:
    # print(task_2())
    # print((task_3()))
    # task_4()
    # task_5()
    # task_6()
    task_7(2, 5)
    pass


if __name__ == '__main__':
    main()
