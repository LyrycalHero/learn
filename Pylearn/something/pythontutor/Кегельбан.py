"""
N кеглей выставили в один ряд, занумеровав их слева направо числами от 1 до N.
Затем по этому ряду бросили K шаров, при этом i-й шар сбил все кегли с номерами от li до ri включительно.
Определите, какие кегли остались стоять на месте.

Программа получает на вход количество кеглей N и количество бросков K.
Далее идет K пар чисел li, ri, при этом 1≤ li≤ ri≤ N.

Программа должна вывести последовательность из N символов, где j-й символ есть “I”,
если j-я кегля осталась стоять, или “.”, если j-я кегля была сбита.
"""
#
# n_skittles, n_throw = input().split(' ')
# n_skittles, n_throw = int(n_skittles), int(n_throw)
# skittles = ['I' for i in range(1, n_skittles + 1)]
# кращє:
n_skittles, n_throw = [int(s) for s in input().split(' ')]
skittles = ['I'] * n_skittles

for number in range(n_throw):
    start, end = [int(s) for s in input().split(' ')]
    for idx in range(start - 1, end):
        skittles[idx] = '.'
print(''.join(skittles))
