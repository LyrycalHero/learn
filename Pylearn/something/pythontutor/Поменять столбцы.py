"""
Дан двумерный массив и два числа: i и j. Поменяйте в массиве столбцы с номерами i и j и выведите результат.

Программа получает на вход размеры массива n и m, затем элементы массива, затем числа i и j.

Решение оформите в виде функции swap_columns(a, i, j).
"""

# n, m = [int(i) for i in input().split()]
# a = []
#
# for i in range(n):
#     a.append([int(j) for j in input().split()])
n, m = 3, 4
a = [[11, 12, 13, 14],
     [21, 22, 23, 24],
     [31, 32, 33, 34]]


def swap_columns(a: list, i, j):
    for row in a:
        row[i], row[j] = row[j], row[i]


swap_columns(a, 0, 1)

for row in a:
    print(' '.join([str(j) for j in row]))


# Рішення розробників
# def swap_columns(a, i, j):
#     for k in range(len(a)):
#         a[k][i], a[k][j] = a[k][j], a[k][i]
#
#
# n, m = [int(i) for i in input().split()]
# a = [[int(j) for j in input().split()] for i in range(n)]
# i, j = [int(i) for i in input().split()]
# swap_columns(a, i, j)
# print('\n'.join([' '.join([str(i) for i in row]) for row in a]))
