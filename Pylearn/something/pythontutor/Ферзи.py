"""
Известно, что на доске 8×8 можно расставить 8 ферзей так, чтобы они не били друг друга.
Вам дана расстановка 8 ферзей на доске, определите, есть ли среди них пара бьющих друг друга.

Программа получает на вход восемь пар чисел, каждое число от 1 до 8 — координаты 8 ферзей. Если ферзи не бьют друг друга,
выведите слово NO, иначе выведите YES.
ввод        вівод
1 7             NO
2 4
3 2
4 8
5 6
6 1
7 3
8 5
"""
x = []
y = []
sum_x_y = []
flag = False

for i in range(8):
    a, b = [int(s) for s in input().split(' ')]
    if i >= 1:
        sum = True if a + b - sum_x_y[i -1] == 2 else False
    if (a in x) or (b in y) or (a + b in sum_x_y) or sum:
        flag = True
        break
    x.append(a)
    y.append(b)
    sum_x_y.append(a + b)
print('YES') if flag else print('NO')

"""
Рішення розробників: 

n = 8
x = []
y = []
for i in range(n):
    new_x, new_y = [int(s) for s in input().split()]
    x.append(new_x)
    y.append(new_y)

correct = True
for i in range(n):
    for j in range(i + 1, n):
        if x[i] == x[j] or y[i] == y[j] or abs(x[i] - x[j]) == abs(y[i] - y[j]):
            correct = False

if correct:
    print('NO')
else:
    print('YES')
"""
