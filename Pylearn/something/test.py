import numpy as np

# Вкажіть розміри тривимірного масиву n
n = 5
a, b, c = n, n, n  # що еквівалентно 5 * 5 * 5

# Створення одновимірного масиву з елементами від 0 до a*b*c-1
one_d_array = np.arange(a * b * c)

# Перетворення одновимірного масиву на тривимірний масив з розмірами (a, b, c)
array_3d = one_d_array.reshape(a, b, c)



for layer in array_3d:
    tab = n - 1
    for row in layer:
        print('\t' * tab + ' '.join([str(i) for i in row]))
        tab -= 1
    print()
#
# Доступ до окремого елементу
# element = array_3d[0, 1, 2]
# print("Окремий елемент:", element)
#
# # Доступ до всього першого "шару" (першого 2D масиву)
# first_layer = array_3d[0, :, :]
# print("Перший шар:")
# print(first_layer)
#
# # Доступ до всього стовпця в усіх шарах
# column = array_3d[:, :, 1]
# print("1 cтовпець в усіх шарах:")
# print(column)
#
# transposed_array = np.transpose(array_3d, (1, 0, 2))
# print("Транспонований масив:")
# print(transposed_array)
