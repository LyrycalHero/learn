'''

Огєєнко П.Ю. лр 1 в глові
лр 2, 3 варанти 1 та 8

3. Дано расстояние в сантиметрах. Найти число полных метров в нем.
4. С некоторого момента прошло 234 дня. Сколько полных недель прошло за этот период?
5. Написать программу, вычисляющую среднее арифметическое двух чисел.
6. Написать программу, которая вычисляет квадрат любого, введенного числа.
7. Дан прямоугольник с размерами 543 x 130 мм. Сколько квадратов со стороной 130 мм можно отрезать от него?
1. Ввести три числа и выведите на экран значение суммы и произведения этих чисел.

2. Ввести с клавиатуры число в диапазоне от 100 до 100000000 (введенное число проверяется). Подсчитать количество четных и нечетных цифр в этом числе в процентном отношении.

3. Ввести с клавиатуры номер трамвайного билета (6-значное число) и проверить является ли данный билет счастливым.

4. Дано трехзначное число. Найти число, полученное при прочтении его цифр справа налево.

5. Дано трехзначное число. Найти число, полученное при перестановке первой и второй цифр заданного числа.

6. Дано двузначное число. Найти:
    а) число десятков в нем;
    б) число единиц в нем;
    в) сумму его цифр;
    г) произведение его цифр.

7. Дано трехзначное число. Найти:
    а) число единиц в нем;
    б) число десятков в нем;
    в) сумму его цифр;
    г) произведение его цифр.

8. Вычислить значение логического выражения при следующих значениях логических величин X, Y и Z: X = Ложь, Y = Истина, Z = Ложь: а) X или Z; б) X и Y; в) X и Z.

9.Вычислить значение логического выражения при следующих значениях логических величин X, Y и Z: X = Ложь, Y = Ложь, Z = Истина: а) X или Y и не Z; г) X и не Y или Z; б) не X и не Y; д) X и (не Y или Z); в) не (X и Z) или Y; е) X или (не (Y или Z)).

'''
import cmath


#поменять 2 числа местами без использования дополнительной переменной
my_a = int(input("ведите А: "))
my_b = int(input("ведите B: "))
my_a += my_b
my_b = my_a - my_b
my_a -= my_b
print(f"А и В поменяли местами: {my_a}, {my_b}")

'''
Створіть дві змінні first=10, second=30.
    Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.'''
first_num = 10
second_num = 3
print(f'Первое число: {first_num}\n'
      +f'Второе число: {second_num}\n'
       f'{first_num} + {second_num} = {first_num + second_num}\n'
       f'{first_num} - {second_num} = {first_num - second_num}\n'
       f'{first_num} * {second_num} = {first_num * second_num}\n'
       f'{first_num} / {second_num} = {first_num / second_num}\n'
       f'Остаток от деления {first_num} на {second_num} = {first_num % second_num}\n'
       f'Целое число от деления {first_num} на {second_num} = {first_num // second_num}\n'
       f'{first_num} в степени {second_num} = {first_num ** second_num}\n')

'''Cтворіть змінну і запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання
    1. Виведіть на екран результат кожного порівняння.'''
my_bool = bool
print("Создали переменную ", my_bool)     #интересно было что выведет
my_bool = first_num < second_num
print(f'{first_num} < {second_num} = {my_bool}')
my_bool = first_num > second_num
print(f'{first_num} > {second_num} = {my_bool}')
my_bool = first_num == second_num
print(f'{first_num} == {second_num} = {my_bool}')
my_bool = first_num != second_num
print(f'{first_num} != {second_num} = {my_bool}')


# Задачі Огієнко 2 лабораторка варіант 1
#Огієнко лаботарона 2 варіант 1
my_x = 3.14
my_y = -27
my_z = -23.2
my_result = 1.2*my_x - 1.3*my_x/my_y + ((1.1%my_y) + 1.2*my_z/my_x -12)/0.8
print(my_result)
my_result = round(my_result, 2)
print(my_result)

#Огієнко лаботарона 3 варіант 1 та округлити до найближчого більшого
my_x = int(input("Введіть х: "))
my_result = ( cmath.cos(my_x)**2 + cmath.atan(my_x)**3 - abs(my_x) ) / (my_x**1.7 + 5)
print(my_result)

#3. Дано расстояние в сантиметрах. Найти число полных метров в нем.
my_number = int(input("Введите сантиметры: "))
print(f'{my_number} = {my_number/100} метров.')

#4. С некоторого момента прошло 234 дня. Сколько полных недель прошло за этот период?
print(f'С некоторого момента прошло 234 дня. Это {234 // 7} полных недель ')

#5. Написать программу, вычисляющую среднее арифметическое двух чисел.
my_number = int(input("Введите первое число: "))
my_second_number = int(input("Введите второе число: "))
my_result = (my_number + my_second_number) / 2
print(f'Среднее арифметечискеое {my_number} и {my_second_number} = {my_result}')

#6. Написать программу, которая вычисляет квадрат любого, введенного числа.
my_number = int(input("Введите число: "))
print(f'Квадрат числа {my_number} = {my_number**2}')

#7. Дан прямоугольник с размерами 543 x 130 мм. Сколько квадратов со стороной 130 мм можно отрезать от него?
print(f'Дан прямоугольник с размерами 543 x 130 мм.\n'
      f'С него можно отрезать {543//130} квадратов со стороной 130 мм')

#1. Ввести три числа и выведите на экран значение суммы и произведения этих чисел.
my_number = int(input("Введите первое число: "))
my_second_number = int(input("Введите второе число: "))
my_third_number = int(input("Введите третье число: "))
print(f'Сумма чисел {my_number}, {my_second_number} и {my_third_number} равна {my_number+my_second_number+my_third_number}\n'
      f'Произведение  этих чисел равно {my_number * my_second_number * my_third_number}')


#4. Дано трехзначное число. Найти число, полученное при прочтении его цифр справа налево.
my_number = int(input("Введите трехзначное число: "))   # без проверки точно ли трехначное
my_result = my_number // 100
my_result += (my_number % 100 // 10 ) * 10
my_result += (my_number % 10) * 100
print("Число, полученное при прочтении его цифр справа налево = ", my_result)

#5. Дано трехзначное число. Найти число, полученное при перестановке первой и второй цифр заданного числа.

'''6. Дано двузначное число. Найти:
    а) число десятков в нем;
    б) число единиц в нем;
    в) сумму его цифр;
    г) произведение его цифр. '''
my_number = int(input("Введите двухзначное число: "))
my_first_number = my_number // 10
my_second_number = my_number % 10
print(f'В нем {my_first_number} десятков и {my_second_number} единиц\n'
      f'Сумма цифр составляет {my_first_number + my_second_number}\n'
      f'Произведение цифр составляет {my_first_number * my_second_number}')


''' 7. Дано трехзначное число. Найти:
    а) число единиц в нем;
    б) число десятков в нем;
    в) сумму его цифр;
    г) произведение его цифр. '''
my_number = int(input("Введите трехзначное число: "))
my_first_number = my_number % 100 // 10
my_second_number = my_number % 10
print(f'В нем {my_first_number} десятков и {my_second_number} единиц\n'
      f'Сумма цифр составляет {my_first_number + my_second_number + (my_number//100)}\n'
       f'Произведение цифр составляет {my_first_number * my_second_number * (my_number//100)}')


'''8. Вычислить значение логического выражения при следующих значениях логических величин X, Y и Z: X = Ложь, Y = Истина, Z = Ложь: 
а) X или Z; б) X и Y; в) X и Z.'''
my_x = True
my_y = True
my_z = False
print("X = Истина, Y = Истина, Z = Ложь")
print("X или Z", my_x or my_z)
print("X и Y", my_x and my_y)
print("X и Z", my_x and my_z)

'''9.Вычислить значение логического выражения при следующих значениях логических величин X, Y и Z: X = Ложь, Y = Ложь, Z = Истина: 
а) X или Y и не Z; б) не X и не Y; 
'''
print("X = Ложь, Y = Ложь, Z = Истина")
my_x = False
my_y = False
my_z = True
print("а) X или Y и не Z", (my_x or my_y) and (not my_z) )
print("б) не X и не Y", (not my_x) and (not my_y))





print("\nКонец файла")
input()
