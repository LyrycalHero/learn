#
#

#ввід 3х чисел
#варіант 1 Вивести на конслоь значеня в порядку зростання
my_a = int(input("Введите 3 числа: "))
my_b = int(input())
my_c = int(input())
my_temp = 0

if my_a > my_b :    #если а больше б меняем местами
    my_temp = my_a
    my_a = my_b
    my_b = my_temp
if my_b > my_c :    #если б больше с меняем местами
    my_temp = my_b
    my_b = my_c
    my_c = my_temp
if my_a > my_b :    #если а больше б меняем местами еще раз после того как поменяли смотрим
    my_temp = my_a
    my_a = my_b
    my_b = my_temp

my_result = f"Числа в порядке возрастания: {my_a}, {my_b}, {my_c}"
print(my_result)

#. Дано целое число.
# Если оно является положительным, то прибавить к нему 1; если отрицательным, то вычесть из него 2; если нулевым, то заменить его на 10.
# Вывести полученное число.
my_a = int(input("Введите целое число: "))
if my_a > 0 :
    my_a += 1
elif my_a <0 :
    my_a -= 2
else:
    my_a = 10
print(my_a)



#7. Даны три целых числа. Найти количество положительных и количество отрицательных чисел в исходном наборе.
my_a = int(input("Введите 3 числа: "))
my_b = int(input())
my_c = int(input())
my_count_pol = 0
my_count_otr = 0
if my_a > 0 :
    my_count_pol +=1
elif my_a < 0 :
    my_count_otr += 1
if my_b > 0 :
    my_count_pol +=1
elif my_b < 0 :
    my_count_otr += 1
if my_c > 0 :
    my_count_pol +=1
elif my_c < 0 :
    my_count_otr += 1


print(f"Количество положительных чисел наборе: {my_count_pol}\n"
      f"Количество отрицательных чисел наборе: {my_count_otr}")



#Даны две переменные целого типа: A и B.
# Если их значения не равны, то присвоить каждой переменной сумму этих значений, а если равны,
# то присвоить переменным нулевые значения. Вывести новые значения переменных A и B.
my_a = int(input("Введите 2 числа: "))
my_b = int(input())
if my_a != my_b :
    my_a = my_b = my_a+ my_b
else:
    my_a = my_b = 0
print(my_a , my_b)

#Напишите программу, проверяющую число на четность.
my_a = int(input("Введите число: "))
if (my_a % 2) == 0 :
    print("Число четное!")
else:
    print("Число нечетное!")

#  Единицы длины пронумерованы следующим образом:
#  1 — дециметр, 2 — километр, 3 — метр, 4 — миллиметр, 5 — сантиметр.
#  Дан номер единицы длины (целое число в диапазоне 1–5) и длина отрезка в этих единицах (вещественное число).
#  Найти длину отрезка в метрах.
print('Введите число единиц длины\n'
                      '1 — дециметр, 2 — километр, 3 — метр, 4 — миллиметр, 5 — сантиметр.')
my_choice = int(input())
my_a = float(input("Введите длину отрезка: "))
if my_choice == 1:
    print(f"{my_a} дециметров это {my_a* 0.1} метров. ")
elif my_choice == 2 :
    print(f"{my_a} километров это {my_a * 1000} метров. ")
elif my_choice == 3 :
    print(f"{my_a} метров")
elif my_choice == 4 :
    print(f"{my_a} миллиметров это {my_a * 0.001 } метров.")
elif my_choice == 5 :
    print(f"{my_a} сантимеров это {my_a * 0.01 } метров. ")
else:
    print("Вы ввели не выбор больше или меньше 5 я нипанимать")


#Дано целое число в диапазоне 20–69, определяющее возраст (в годах).
#Вывести строку-описание указанного возраста, обеспечив правильное согласование числа со словом «год», например:
#20 — «двадцать лет», 32 — «тридцать два года»,
# 41 — «сорок один год»
my_age = int(input("Введите возраст с 20 по 69 лет: "))
my_age_end = my_age % 10    #узнаем на какую цифру заканчивается возраст
my_result = "Вам {} {} "


if my_age <20 or my_age > 69:
    print("Ввы ввели число не в диапазоне 20 - 69")
else:
    if my_age_end == 0 or (my_age_end >=5 and my_age_end <= 9)  :
        my_result = my_result.format(my_age, "лет")
    elif my_age_end == 1:
        my_result = my_result.format(my_age, "год")
    elif my_age_end >=2 and my_age_end <= 4 :
        my_result = my_result.format(my_age, "года")
    print(my_result)

#14. Среди трех чисел найти среднее. Если среди чисел есть равные, вывести сообщение "Ошибка".
my_a = int(input("Введите 3 числа: "))
my_b = int(input())
my_c = int(input())
my_result = "Среднее среди чисел : {}"
if my_a == my_b or my_b == my_c or my_a == my_c :
    print("Ошибка")
    # легший спосіб
else:
   if (my_b < my_a < my_c) or (my_c < my_a < my_b):
       print(my_result.format(my_a))
   elif (my_a < my_b < my_c) or (my_c < my_b < my_a):
       print(my_result.format(my_b))
   elif (my_a < my_c < my_b) or (my_b < my_c < my_a):
       print(my_result.format(my_c))
                '''старий спосіб 
                else:
                if (my_a < my_b and my_a > my_c) or (my_a < my_c and my_a > my_b) :
                    print(my_result.format(my_a))
                elif (my_b < my_c and my_b > my_a) or (my_b < my_a and my_b > my_c) :
                    print(my_result.format(my_b))
                elif (my_c < my_b and my_c > my_a) or (my_c<my_a and my_c > my_b):
                    print(my_result.format(my_c))
                '''





#15. Определить, является ли число а делителем числа b
my_a = int(input("Введите 2 числа: "))
my_b = int(input())
if my_b % my_a == 0 :
    print(f"Число {my_a} являеться делителем числа {my_b}")
else:
    print(f"Число {my_a} НЕ являеться делителем числа {my_b}")



#16. Проверить, принадлежит ли число введенное с клавиатуры, интервалу (-5;3).
my_a = int(input("Введите число: "))
if my_a > - 5 and my_a < 3 :
    print(f"Да, {my_a} приналдежит орезку (-5;3)")
else:
    print(f"Нет, {my_a} не приналдежит орезку (-5;3)")

#Дано двузначное число. Определить: входит ли в него цифра 3
my_a = int(input("Введите двухзначное число: "))
if my_a// 10 ==3 or my_a% 10 == 3 :
    print(f"Да, в {my_a} входит число 3")
else:
    print(f"Нет, в {my_a}  не входит число 3")

#Дано слово. Вывести на экран его третий символ.
# Определить, одинаковы ли второй и четвертый символы в нем.
#Дано слово s1. Получить слово s2, образованное нечетными буквами слова s1
my_str  = input("Введите строку: ")     #яскраво видно якщо ввести "123456789"
#Дано слово s1. Получить слово s2, образованное нечетными буквами слова s1
my_result = my_str[1:len(my_str):2]     #Дано слово s1. Получить слово s2, образованное нечетными буквами слова s1
print(my_result)
print("3й символ строки: ",my_str[2])        #Вывести на экран его третий символ.
if my_str[1] == my_str[3]:                     #одинаковы ли второй и четвертый символы в нем.
    print("Да, второй и четвертій символы одинаковы")
else:
    print("Нет, второй и четвертій символы не одинаковы")


