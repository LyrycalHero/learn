'''
Підготовка до ЛР 5
Псевдографіка
'''

# перший просто прямокутний трикутник на 4 рядки та 4 стовпчика
for i in range(1,4+1):          # 4 рядки + 1 бо треба щоб 4 включалось
    for j in range(i):      # стовбчики
        print('X',end = "")
    for j in range(4-i):
        print(" ", end = "")
    print()                     #переход на нову строку

print()

# другий прямокутник ніби рівнобедрений 4 рядки але 7 стовпчиків
for i in range(1,4+1):                      #рядки
    for j in range(int((7-(2*i-1))/2)):
        print(" ", end = "")
    for j in range(i*2-1):
        print("X",end = "")
    print()

print()

# третій 2 рівнобедрених трикутника  4 рядки
#виходить 2 по 7 стовбчиків + 2 стовчика між ними
for i in range (1,4+1):
    for j in range(int((7 - (2 * i - 1)) / 2)):
        print(" ", end="")
    for j in range(i * 2 - 1):
        print("X", end="")
    for j in range(11 -i*2 -1):
        print(" ", end="")
    for j in range(i * 2 - 1):
        print("X", end="")
    print()
