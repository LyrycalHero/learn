'''
Псевдографіка циклами
виконання прикладу і порівняння з кодом
спершу на аркуші в клітинку побудував зображеня для комфортно рахування рядків
'''

# ширина фігури виходить 35 символів див малюнок
# розбито на 6 циклів по рядкам див. малюнок

# 1й цикл верх
for i in range(1,2+1):                      #рядки
    for j in range(19-i):
        print(" ", end = "")
    for j in range(i*2-1):
        print("X",end = "")
    print()
#2й
for i in range(1,2+1):
    for j in range(9 - i):
        print(" ", end="")
    for j in range(i * 2 - 1):
        print("X", end="")
    for j in range(9 -i*2):
        print(" ", end="")
    for j in range(i*2+3):
        print("X", end="")
    for j in range(9 -i*2):
        print(" ", end="")
    for j in range(i * 2 - 1):
        print("X", end="")
    print()

#3й
for i in range(1,2+1):
    for j in range(7-i):
        print(" ", end="")
    for j in range(3+2*i):
        print("X", end="")
    for j in range(5-i):
        print(" ", end="")
    for j in range(7):
        print("X", end="")
    for j in range(5 - i):
        print(" ", end="")
    for j in range(3+2*i):
        print("X", end="")
    print()

#4й все просто 5 пробела 7х 3 про 7х 3 пробілу 7х
for i in range(1, 2 + 1):
    for j in range(5):
        print(" ", end="")
    for j in range(7):
        print("X", end="")
    for j in range(3):
        print(" ", end="")
    for j in range(7):
        print("X", end="")
    for j in range(3):
        print(" ", end="")
    for j in range(7):
        print("X", end="")
    print()
#5й 5 строк
for i in range(5):
    for j in range(5):
        print(" ", end="")
    for j in range(27):
        print("X", end="")
    print()
#6й 4 строчки
for i in range(1,4+1):
    for j in range(5 - i):
        print(" ", end="")
    for j in range(27 + i* 2):
        print("X", end="")
    print()

input()