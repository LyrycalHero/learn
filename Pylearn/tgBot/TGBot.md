Для початку роботи треба встановити модуль pyTelegramBotApi

В Пайчармі File - Settings - Project: Python - Python Interpriter
Далі - import telebot

В самому телеграмі офіційний бот @BotFather дозволяє реєструвати нових ботів

Мануал - https://core.telegram.org/bots

Мій бот - https://t.me/myTestLearnBot1122_bot

Токен для звернення на сервер телграму Use this token to access the HTTP API:
6925142536:AAEOFMFpDlNF4tC2fDzQ6S39bwALhYuUcfg


створення зміної для роботи з ботом:

    <ім'я зміної> = telebot.TeleBot('<токен>')
приклад:

    bot = telebot.TeleBot('6925142536:AAEOFMFpDlNF4tC2fDzQ6S39bwALhYuUcfg')

Ф-я яка постійно звертається до серверу тг та дізнається чи не написали в бот щось,
але мінус в тому, що через 30-40 хв сервера тг скине з'єднання з ботом. Завжди має бути в кінці коду
Це 1 з 3х варіантів ботів і найпростіший (Пулінг), також є ассинхроні та вебхук

    bot.polling()

Функція декоратор, яка задає тип месенджу, фяий  сприймає наш бот
    
    @bot.message_handlers(commands = ['<текстові команди>', 'start', 'help'])

Коротка перевірка чи працює бот (при надсиланні "/start" бот буде вітатись : 

    
    import telebot

    bot = telebot.TeleBot('6925142536:AAEOFMFpDlNF4tC2fDzQ6S39bwALhYuUcfg')     #створення зміної для роботи з ботом
    
    @bot.message_handler(commands = ['start', 'help'])
    #ф-я яка буде реагувати на те, що хтось напише боту
    # зміна message містить меседж серверу телеграм, а саме хто написав, що написав і тощо
    def get_commands(message):
        if message.text == '/start':
            bot.send_message(message.chat.id, 'Привіт, йопта \U0001F601')   #\U0001F601 == 😁
    
    bot.polling()   #Ф-я яка постійно звертається до серверу тг та дізнається чи не написали в бот щось,
                    #але мінус в тому, що через 30-40 хв сервера тг відключать бот. Завжди має бути в кінці коду


В боті клавіатури бувають 2х видів: 
 - підекранні (виводиться під полем вводу тексту) бібліотека types 
 - Інлайн (ті які в історії меседжу, тобто в листуванні )

Підекранна клавіатура: 

    from telebot import types
    #об'єкто головної підекранної клавіатури
    main_keyboard = types.ReplyKeyboardMarkup().add('Каталог', 'Поділитися ботом')
де types.ReplyKeyboardMarkup(resize_keyboard=True) - просто сворює підекранну клавіатуру, за замовчуванням вона товста

resize_keyboard=True - робить тонкі кнопки 

.add('Каталог', 'Поділитися ботом') - додає 2 кнопки. Максимум 3 кнопки! в рядку

Інлайн кнопки так само створюємо через types, який імпортован з telebot
    
        inl_keyb = types.InlineKeyboardMarkup()             #об'єкт інлайн клавіатури

        # додавання кнопочок в інлайн клавіатуру додає в 1 рядок
        inl_keyb.add(types.InlineKeyboardButton('Каталог 1', callback_data='cat1'),
                     types.InlineKeyboardButton('Каталог 2', callback_data='cat2'))
        #додавання кнопочки в 2й рядок 
        inl_keyb.add(types.InlineKeyboardButton('Каталог 3', callback_data='cat3')
        #вивід клавіатури користувачу
        bot.send_message(message.chat.id,
                         'Оберіть який каталог товарів цікавить:',
                         reply_markup=inl_keyb)
Де callback_data=  - це запрос на сервер, який користувач не бачить


Декоратор обробки тицнення однієї з кнопок підекранної клавіатури (текстового месседжу)
через content_types='text'

Оброка тексту, що вводить користувач але саме команд, тобто через /: 

    # декоратор обробки месседж команд
    @bot.message_handler(commands = ['start', 'help'])
    def get_commands(message):
        if message.text == '/start':
            bot.send_message(message.chat.id, 'Вітаю', reply_markup=main_keyboard))
де змінна message містить дофіга інфи, а саме message.text, або ІД чату 
reply_markup= - викликає підекранну клавіатуру 

Декоратор обробки тицнення інлайн кнопок! Dже працюємо не з об'єктом message, 
а з об'єктом query який теж містить дофіга інфи і теж словник
тут логіка обробка будь-якої інлайн кнопки ! 

    @bot.callback_query_handlers(lambda a: True)
    def get_query(query):
    #Обробка події натицення кнопки 'Каталог 1', callback_data='cat1'
        if query.data == 'cat1':
            inl_keyb = types.InlineKeyboardMarkup()  # об'єкт нової інлайн клавіатури
            inl_keyb.add(types.InlineKeyboardButton('підкаталог каталогу 1', callback_data='cat1_cat2'),
            ...
            # вивід клавіатури користувачу через query.message!
            bot.send_message(query.message.chat.id,
                         'Оберіть підкатегорію'
                         reply_markup=inl_keyb)
        elif query.data == 'cat1_cat2':
            ...

Надіслати фото де open(<шлях до зображення>, "rb")
    
    bot.send_photo(message.chat.id, open("res/cat.jpg", "rb"))

Кастомна клавіатура 
Як зробити меню як в Альберта? - через ботфазер 

вспливаюча підказка в чаті

        bot.answer_callback_query(query.id, 'Товар придбано!') 

видалення з чату(chat_id) повідомлення яке має id_mes також мона видаляти меседжи з кнопками 

        bot.delete_message(<chat_id>, <id_mes>) 

Можна додатково почитати https://surik00.gitbooks.io/aiogram-lessons/content/chapter5.html