# Модуль Neuron
import numpy as np


# Создание класса "Нейрон"
class Neuron:
    def __init__(self, w):
        self.w = w

    def у(self, х):  # Сумматор
        s = np.dot(self.w, х)  # Суммируем входы
        return s  # функция активации


Xi = np.array([1, 0, 0, 1])  # Задание значений входам
Wi = np.array([5, 4, 1, 1])  # Веса входных сенсоров
n = Neuron(Wi)  # Создание объекта из класса Neuron
print('S1= ', n.у(Xi))  # Обращение к нейрону1