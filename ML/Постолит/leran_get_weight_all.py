# 0 = '111101101101111'
# 1 = '001001001001001'
# 2 = '111001111100111'
# 3 = '111001111001111'
# 4 = '101101111001001'
# 5 = '111100111001111'
# 6 = '111100111101111'
# 7 = '111001001001001'
# 8 = '111101111101111'
# 9 = '111101111001111'
import random

numO = [int(i) for i in '111101101101111']
num1 = [int(i) for i in '001001001001001']
num2 = [int(i) for i in '111001111100111']
num3 = [int(i) for i in '111001111001111']
num4 = [int(i) for i in '101101111001001']
num5 = [int(i) for i in '111100111001111']
num6 = [int(i) for i in '111100111101111']
num7 = [int(i) for i in '111001001001001']
num8 = [int(i) for i in '111101111101111']
num9 = [int(i) for i in '111101111001111']

# Список всех цифр от О до 9 в едином массиве
nums = [numO, num1, num2, num3, num4, num5, num6, num7, num8, num9]

n_sensor = 15  # количество сенсоров n_sensor == len(num0)

# Початковий списов вагів заповнений 0 для кожної цифри
weights = []
for row in range(10):  # 10 бо в нас 10 цифр від 0 до 9
    weights.append([0] * 15)


# Функция определяет, является ли полученное изображение числом 5
# Возвращает Да, если признано, что это 5. Возвращает Нет, если отвергнуто, что это 5
def perceptron(sensor: list, tema):
    b = 7  # Порог функции активации (ми рандомно вирішили що нехай це буде 7)
    sum = 0
    for i in range(n_sensor):
        sum += sensor[i] * weights[tema][i]

    return True if sum >= b else False


# Уменьшение значений весов
# Если сеть ошиблась и выдала Да при входной цифре, отличной от пятерки
def decrease(number: list, tema):
    for i in range(n_sensor):
        if number[i] == 1:  # Возбужденный ли вход
            weights[tema][i] -= 1  # Уменьшаем связанный с входом вес на единицу


# Увеличение значений весов
# Если сеть не ошиблась и выдала Да при поданной на вход цифре 5
def increase(number: list, tema):
    for i in range(n_sensor):
        if number[i] == 1:  # Возбужденный ли вход
            weights[tema][i] += 1  # Увеличиваем связанный с входом вес на единицу


# Тренировка сети
for tema in range(10):  # буде поступово тренувати кожну цифру від 0 до 9
    n = 1000  # количество уроков
    for i in range(n):
        j = random.randint(0, 9)
        # Результат обращения к сумматоруь ответ True или False
        r = perceptron(nums[j], tema)

        if j != tema:  # Если генератор вьщал случайное число j, не равное tema
            if r:  # Если сумматор сказал Да (это tema), а j это не tema.
                decrease(nums[j], tema)  # # наказываем сеть (уменьшаем значения весов в weights)
        else:  # Если генератор відал j == tema
            if r == False:  # Если сумматор сказал Нет
                # підвищуємо вагу збуджених нейронів до тих пір поки не почне на 5ку видавати True
                increase(nums[j], tema)  # увеличиваем значения весов

    print(f'навчали число: {tema}, коефіцієнти = {weights[tema]}')

# проверка работы программы на обучающей выборке

print("3 это 5?", perceptron(num3, 5))
print("4 это 5?", perceptron(num4, 5))
print("5 это 5?", perceptron(num5, 5))

print("3 это 0?", perceptron(num3, 0))
print("4 это 0?", perceptron(num4, 0))
print("0 это 0?", perceptron(numO, 0))

print("3 это 1?", perceptron(num3, 1))
print("4 это 1?", perceptron(num4, 1))
print("1 это 1?", perceptron(num1, 1))

...
