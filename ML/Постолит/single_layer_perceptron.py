# Искусственный нейрон (персептрон)
def perceptron(sensor: list, weights: list):
    b = 7  # порог ф-її активації
    sum = 0  # початкове значення суми
    connections = len(sensor)  # кількість зв'язків  S-A
    # цикл суммирования сигналов от сенсоров
    for i in range(connections):
        sum += int(sensor[i]) * weights[i]

    return True if sum >= b else False


# Проверка работы искусственного нейрона (персептрона
num1 = list('001001001001001')
num2 = list('111001111100111')
weights = [1 for i in range(15)]  # Присвоение значений всем связям w=

print(num1)
print(perceptron(num1, weights))
print(num2)
print(perceptron(num2, weights))
