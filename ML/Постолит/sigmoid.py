# Модуль onestep
import numpy as np

# функция активации: f(x) = 1 / (1 + е ** (-х)
def sigmoid(x):
    return 1 / (1 + np.exp(-x))


# Создание класса "Нейрон"
class Neuron:
    def __init__(self, w):
        self.w = w

    def у(self, х):  # Сумматор
        s = np.dot(self.w, х)  # Суммируем входы
        return sigmoid(s)  # функция активации

Xi = np.array([1, 0, 0, 1])  # Задание значений входам
Wi = np.array([5, 4, 1, 1])  # Веса входных сенсоров
n = Neuron(Wi)  # Создание объекта из класса Neuron
print('Благоприятно=', n.у(Xi))  # Обращение к нейрону1

# Неблагоприятніе условия 0, 0, 0, 0
Xi = np.array([0, 0, 0, 0])
print('Неблагоприятно: ', n.у(Xi))

