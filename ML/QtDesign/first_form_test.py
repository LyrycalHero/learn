from PyQt5 import QtWidgets, uic
import sys

app = QtWidgets.QApplication(sys.argv)

window = uic.loadUi('first_test.ui')
# виводимо на монітор створену в дизайнері форму
window.show()
# основний цикл додатку і обробка поідй починається з цієї точки
sys.exit(app.exec_())
